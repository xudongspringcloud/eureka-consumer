package com.example.eurekaconsumer.controller;

import com.example.eurekaconsumer.dao.OrderMapper;
import com.example.eurekaconsumer.dao.ProductMapper;
import com.example.eurekaconsumer.dao.UserMapper;
import com.example.eurekaconsumer.model.ProductInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/order")
public class OrderController {
    @Resource
    UserMapper userMapper;
    @Resource
    OrderMapper orderMapper;
    @Resource
    ProductMapper productMapper;

    @Autowired
    LoadBalancerClient loadBalancerClient; // 根据服务名称，负载均衡选择服务方
    @Autowired
    RestTemplate restTemplate;

    @RequestMapping(value = "/deal",method = RequestMethod.POST)
    public String dealOrder(Model model, @RequestParam("productId") String productId,
               @RequestParam("price") String price,@RequestParam("sum") String sum){
        Map<String,String> map = new HashMap<>();
        map.put("productId",productId);
        map.put("sum",sum);
        // 调用库存服务，更新库存信息
        ServiceInstance serviceInstance = loadBalancerClient.choose("eureka-client");
        String url = "http://" + serviceInstance.getHost() + ":" + serviceInstance.getPort() + "/productRepo";
        Object obj = restTemplate.getForObject(url, String.class);
        System.out.println(" 收到"+url+"远程调用返回的结果："+obj);
        // 调用积分服务，更新积分信息（待添加）

        // 处理需求购买一个苹果手机，需要 更新库存信息和 个人积分信息
        List<ProductInfo> productInfoList = productMapper.selectAll();
        model.addAttribute("productInfoList",productInfoList);
        return "index";
    }

}
