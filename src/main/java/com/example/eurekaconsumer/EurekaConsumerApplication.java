package com.example.eurekaconsumer;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@EnableDiscoveryClient // 将当期应用加入到服务治理体系中
@SpringBootApplication
@MapperScan("com.example.eurekaconsumer.dao")
public class EurekaConsumerApplication {
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
    public static void main(String[] args) {
       // SpringApplication.run(EurekaConsumerApplication.class, args);
        new SpringApplicationBuilder(EurekaConsumerApplication.class).web(true).run(args);

    }

}
