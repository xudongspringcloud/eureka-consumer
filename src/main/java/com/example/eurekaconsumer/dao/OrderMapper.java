package com.example.eurekaconsumer.dao;

import com.example.eurekaconsumer.model.OrderInfo;

import java.util.List;

public interface OrderMapper {
    List<OrderInfo> selectAll();

}
