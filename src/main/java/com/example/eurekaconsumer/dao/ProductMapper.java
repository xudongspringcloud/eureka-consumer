package com.example.eurekaconsumer.dao;

import com.example.eurekaconsumer.model.ProductInfo;

import java.util.List;

public interface ProductMapper {
    List<ProductInfo> selectAll();

}
